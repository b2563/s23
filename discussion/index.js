//console.log("Hello World")

/*
    - An object is a data type that is used to represent real world objects
    - It is a collection of related data and/or functionalities
    - In JavaScript, most core JavaScript features like strings and arrays are objects (Strings are a collection of characters and arrays are a collection of data)
    - Information stored in objects are represented in a "key:value" pair
    - A "key" is also mostly referred to as a "property" of an object
    - Different data types may be stored in an object
    - Different data types may be stored in an object's property creating complex data structures
*/

// Creating objects using object initializers/literal notation

let cellphone = {


    // key: value
    name: 'Nokia 3210',
    manufactureDate: 1999

};

console.log('Result from creating objects using initializers/literal notation:');
console.log(cellphone);
console.log(typeof cellphone);

//Creating objects

function laptop(name, manufactureDate) {

    this.name = name;
    this.manufactureDate = manufactureDate;

};

//Create an instance of an object
let myLaptop = new laptop('Lenovo', 2008);
console.log('Result from creating objects using object constructors:');
console.log(myLaptop);

let Laptop1 = new laptop('Macbook Air', 2020);
console.log('Result from creating objects using object constructors:');
console.log(Laptop1);

let oldLaptop = new laptop('Portal R2E CCMC', 1980);
console.log('Result from creating objects using object constructors:');
console.log(oldLaptop);

// Creating empty objects
let computer = {};
let myComputer = new Object();

console.log(computer);
console.log(myComputer);

//[Section] Accessing Object Properties

//Using dot notation
console.log('Result from dot notation: ' + myLaptop.name)

//Using the square bracket notation
console.log('Result from the square bracket notation: ' + myLaptop['name']);

//Accessing array objects

let array = [myLaptop, Laptop1]
console.log(array);

//Using dot notation
console.log(array[0].name);

//Using square bracket notation
console.log(array[0]['name']);

// [Section] Initializing

let car = {};

//Initializing/Adding using dot
car.name = 'Honda Civic';
console.log('Result from adding properties using dot notation: ');
console.log(car);

//Initializing/Adding using square bracket notation
car['manufactureDate'] = 2019;
console.log('Result from adding properties using bracket notation: ');
console.log(car);

delete car.manufactureDate;
console.log('Result from deleting properties: ');
console.log(car);

//Reassigning object
car.name = 'Dodge Challenger R/T';
console.log('Result from reassigning properties: ');
console.log(car);

//[Section] Object Method
let person = {
    name: 'John',
    //method
    talk: function(){

        console.log("Hello my name is " + this.name);
    }
}

//Adding methods to objects
console.log(person);
console.log('Result of object methods:');
person.talk();

person.walk = function() {
    console.log(this.name + " walked 25 steps forward");
}
person.walk();

let friend = {
	firstName: "John",
	lastName: "Doe",
	address: {
		city: "Austin, Texas",
		country: "US"
	},
	emails: ["johnD@mail.com", "joe12@yahoo.com"],
	introduce: function () {
		console.log("Hello! My name is " + this.firstName + " " + this.lastName)
	}
}

friend.introduce()


















