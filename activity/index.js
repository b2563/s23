let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Balbasaur"],
	friend: {
		friends: ["Brock", "Misty"],
		hoenn: ["May", "Max"],
	},
	talk: function() {
		console.log("Pikachu! I choose you!");
	}
}


console.log(trainer)
console.log(trainer.name);
console.log(trainer['pokemon']);
trainer.talk();

console.log("");

function Pokemon(name, level) {

	this.name = name
	this.level = level
	this.health = 2 * level
	this.attack = level

	
	this.tackle =  function(target) {
	this.faint = function() {
		console.log(target.name + " fainted")
	}
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced " + (target.health - this.attack))
	if ((target.health - this.attack) <=5) {
		this.faint();
	} 
	target.health = target.health - this.attack;
	console.log(target);
	}
}

let Pikachu = new Pokemon("Pikachu", 12);
let Geodude = new Pokemon("Geodude", 8);
let Mewtwo = new Pokemon("Mewtwo", 100);
console.log(Pikachu);
console.log(Geodude);
console.log(Mewtwo);

Geodude.tackle(Pikachu);
Geodude.tackle(Pikachu);
Geodude.tackle(Pikachu);

Mewtwo.tackle(Geodude);







